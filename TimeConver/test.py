def converTime(time):
    h = time[0:2]
    m = time[3:5]
    s = time[6:8]
    status = time[8]
    if time[8] == 'P' and h != '12':
        h = int(h) + 12
        return '{}:{}:{}'.format(h,m,s)
    elif time[8] == 'P' and h == '12':
        return '{}:{}:{}'.format(h,m,s)
    elif time[8] == 'A' and h == '12':
        h = '00'
        return '{}:{}:{}'.format(h,m,s)
    else:
        return '{}:{}:{}'.format(h,m,s)



print(converTime('12:05:45AM'))
